package org.gcube.spatial.data.geonetwork.iso.tpl.parties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Person {

	private String name;
	private String organization;
		
}
