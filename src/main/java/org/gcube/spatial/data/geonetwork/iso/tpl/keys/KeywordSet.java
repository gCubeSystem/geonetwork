package org.gcube.spatial.data.geonetwork.iso.tpl.keys;

import java.util.HashSet;
import java.util.Set;

import org.gcube.spatial.data.geonetwork.iso.tpl.InvalidValueException;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.KeywordType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@AllArgsConstructor
public class KeywordSet {


	
	private KeywordType type;	
	
	private Set<String> values=new HashSet<>();
	private Thesaurus thesaurus;
	
	public void validate() throws InvalidValueException{
		if(values.isEmpty()) throw new InvalidValueException("KeywordSet cannot be empty. Thesaurus is : "+thesaurus);
		if(thesaurus!=null)
			for(String v:values) {
				if(!thesaurus.validate(v))
					throw new InvalidValueException("Invalid value "+v+" for Thesaurus : "+thesaurus);
			}
	}

	public KeywordSet(KeywordType type, Thesaurus thesaurus) {
		super();
		this.type = type;
		this.thesaurus = thesaurus;
	}
	
	public void addKeyword(String toAdd) {
		this.values.add(toAdd);
	}

	public KeywordSet(Set<String> values) {
		super();
		this.values = values;
	}
	
	
}
