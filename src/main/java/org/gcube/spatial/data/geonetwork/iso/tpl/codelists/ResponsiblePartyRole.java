package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponsiblePartyRole {

	RESOURCE_PROVIDER("resourceProvider","Party that supplies the resource"),
	CUSTODIAN("custodian","Party that accepts accountability and responsability for the data and ensures appropriate care and maintenance of the resource"),
	OWNER("owner","Party that owns the resource"),
	USER("user","Party who uses the resource"),
	DISTRIBUTOR("distributor","Party who distributes the resource"),
	ORIGINATOR("originator","Party who created the resource"),
	POINT_OF_CONTACT("pointOfContact","Party who can be contacted for acquiring knowledge about or acquisition of the resource"),
	PRINCIPAL_INVESTIGATOR("principalInvestigator","Key party responsible for gathering information and conducting research"),
	PROCESSOR("processor","Party wha has processed the data in a manner such that the resource has been modified"),
	PUBLISHER("publisher","Party who published the resource"),
	AUTHOR("author","Party who authored the resource");
	
	
	
	
	private String id;
	private String description;
	
	public static ResponsiblePartyRole getById(String id) {
		for(ResponsiblePartyRole t:ResponsiblePartyRole.values())
			if(t.id.equals(id)) return t;
		throw new IllegalArgumentException("No ResponsiblePartyRole with id "+id);
	}
	
}
