package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RestrictionCode {

	COPYRIGHT("copyright","Copyright"),
	PATENT("patent","Patent"),
	PATENT_PENDING("patentPending","Patent pending"),
	TRADEMARK("trademark","Trademark"),
	LICENSE("license","License"),
	INTELLECTUAL_PROPERTY_RIGHTS("intellectualPropertyRights","Intellectual Property Rights"),
	RESTRICTED("restricted","Restricted"),
	OTHER_RESTRICTIONS("otherRestrictions","Other restrictions");
	
	private String id;
	private String label;
	
	
}
