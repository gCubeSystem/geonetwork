package org.gcube.spatial.data.geonetwork.iso.tpl.keys;

import java.util.Date;
import java.util.HashSet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@AllArgsConstructor

public class Thesaurus {
	
	
	public static final Thesaurus INSPIRE_THEMES=new Thesaurus("GEMET - INSPIRE themes, version 1.0", new Date(2008,06,01));
	
	static {
		INSPIRE_THEMES.codelist.add("Addresses");
		INSPIRE_THEMES.codelist.add("Administrative units");
		INSPIRE_THEMES.codelist.add("Cadastral parcels");
		INSPIRE_THEMES.codelist.add("Coordinate reference systems");
		INSPIRE_THEMES.codelist.add("Geographical grid systems"); 	
		INSPIRE_THEMES.codelist.add("Geographical names");
		INSPIRE_THEMES.codelist.add("Hydrography"); 	
		INSPIRE_THEMES.codelist.add("Protected sites");
		INSPIRE_THEMES.codelist.add("Transport networks"); 	
//			ANNEX: 2
		INSPIRE_THEMES.codelist.add("Elevation"); 	
		INSPIRE_THEMES.codelist.add("Geology");
		INSPIRE_THEMES.codelist.add("Land cover"); 	
		INSPIRE_THEMES.codelist.add("Orthoimagery");
//			ANNEX: 3
		INSPIRE_THEMES.codelist.add("Agricultural and aquaculture facilities"); 	
		INSPIRE_THEMES.codelist.add("Area management / restriction / regulation zones & reporting units");
		INSPIRE_THEMES.codelist.add("Atmospheric conditions"); 	
		INSPIRE_THEMES.codelist.add("Bio-geographical regions");
		INSPIRE_THEMES.codelist.add("Buildings");
		INSPIRE_THEMES.codelist.add("Energy Resources");
		INSPIRE_THEMES.codelist.add("Environmental monitoring Facilities"); 	
		INSPIRE_THEMES.codelist.add("Habitats and biotopes");
		INSPIRE_THEMES.codelist.add("Human health and safety"); 	
		INSPIRE_THEMES.codelist.add("Land use");
		INSPIRE_THEMES.codelist.add("Meteorological geographical features"); 	
		INSPIRE_THEMES.codelist.add("Mineral Resources");
		INSPIRE_THEMES.codelist.add("Natural risk zones"); 	
		INSPIRE_THEMES.codelist.add("Oceanographic geographical features");
		INSPIRE_THEMES.codelist.add("Population distribution and demography"); 	
		INSPIRE_THEMES.codelist.add("Production and industrial facilities");
		INSPIRE_THEMES.codelist.add("Sea regions"); 	
		INSPIRE_THEMES.codelist.add("Soil");
		INSPIRE_THEMES.codelist.add("Species distribution"); 	
		INSPIRE_THEMES.codelist.add("Statistical units");
		INSPIRE_THEMES.codelist.add("Utility and governmental services");
	}
	
	
	
	
	

	@NonNull
	private String name;
	@NonNull
	private Date creationDate;
	private HashSet<String> codelist=new HashSet<String>();
	
	public HashSet<String> codelistValues(){
		return (HashSet<String>) codelist.clone();
	}
	
	public boolean validate(String value) {
		if(codelist.isEmpty()) return true;
		return codelist.contains(value);
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Thesaurus [name=" + name + "]";
	}
	
	
}
