package org.gcube.spatial.data.geonetwork.iso.tpl.parties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class Contact{
	@NonNull
	private String email;
	@NonNull
	private String site;
}