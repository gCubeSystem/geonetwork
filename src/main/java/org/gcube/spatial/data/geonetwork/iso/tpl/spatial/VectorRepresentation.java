package org.gcube.spatial.data.geonetwork.iso.tpl.spatial;

import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.GeometricObjectType;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopologyLevel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VectorRepresentation extends SpatialRepresentation {

	@Override
	public Type getType() {
		return Type.VECTOR;
	}

	@NonNull
	private TopologyLevel topologyLevel=TopologyLevel.GEOMETRY_ONLY;
	
	private int geometricObjectCount=0;
	
	@NonNull
	private GeometricObjectType geometricObjectType=GeometricObjectType.POINT;
	
}
