package org.gcube.spatial.data.geonetwork.iso.tpl.constraints;

import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.RestrictionCode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class LegalConstraints {
	
	@NonNull	
	private RestrictionCode code;
	
	
	private String otherRestrictionValue;
	
}
