package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ScopeCode {

	ATTRIBUTE("attribute","Attribute"),
	ATTRIBUTE_TYPE("attributeType","Attribute Type"),
	DATASET("dataset","Dataset"),
	SERIES("series","Series"),
	NON_GEOGRAPHIC_DATASET("nonGeographicDataset","Non Geographic Dataset"),
	FEATURE("feature","Feature"),
	FEATURE_TYPE("featureType","Feature Type"),
	PROPERTY_TYPE("propertyType","Property Type"),
	TILE("tile","Tile");
	
	private String id;
	private String label;
	
	
}
