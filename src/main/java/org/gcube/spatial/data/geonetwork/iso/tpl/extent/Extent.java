package org.gcube.spatial.data.geonetwork.iso.tpl.extent;

import java.util.HashSet;

import org.opengis.metadata.extent.TemporalExtent;
import org.opengis.metadata.extent.VerticalExtent;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class Extent {

	
	private String description;
	@NonNull
	private HashSet<GeographicExtent> geographicExtents=new HashSet<>();
	@NonNull
	private HashSet<TemporalExtent> temporalExtents=new HashSet<>();
	@NonNull
	private HashSet<VerticalExtent> verticalExtents=new HashSet<>();
	
	public void addGeographicExtent(GeographicExtent toAdd) {
		getGeographicExtents().add(toAdd);
	}
	
	public void addTemporalExtent(TemporalExtent toAdd) {
		getTemporalExtents().add(toAdd);
	}
	public void addVerticalExtent(VerticalExtent toAdd) {
		getVerticalExtents().add(toAdd);
	}
}
