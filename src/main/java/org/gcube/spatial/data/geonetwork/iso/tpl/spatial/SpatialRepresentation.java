package org.gcube.spatial.data.geonetwork.iso.tpl.spatial;

public abstract class SpatialRepresentation {

	protected static enum Type{
		VECTOR,GRID,
	}
	
	public abstract Type getType();
	
}
