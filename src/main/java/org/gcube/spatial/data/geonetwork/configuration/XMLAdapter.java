package org.gcube.spatial.data.geonetwork.configuration;

public interface XMLAdapter {

	public String adaptXML(String xml);
	
}
