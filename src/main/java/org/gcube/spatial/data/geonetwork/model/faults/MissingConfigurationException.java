package org.gcube.spatial.data.geonetwork.model.faults;

public class MissingConfigurationException extends ConfigurationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2645967308563239255L;

	public MissingConfigurationException() {
		// TODO Auto-generated constructor stub
	}

	public MissingConfigurationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MissingConfigurationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MissingConfigurationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MissingConfigurationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
