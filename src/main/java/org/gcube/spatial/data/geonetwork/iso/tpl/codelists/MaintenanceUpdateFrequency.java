package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MaintenanceUpdateFrequency {

	CONTINUAL("continual","Continual"),
	DAILY("daily","Daily"),
	WEEKLY("weekly","Weekly"),
	FORTNIGHTLY("fortnightly","Fortnightly"),
	MONTHLY("monthly","Monhtly"),
	QUARTERLY("quarterly","Quarterly"),
	BIANNUALLY("biannually","Biannually"),
	ANNUALLY("annually","Annually"),
	AS_NEEDED("asNeeded","As needed"),
	IRREGULAR("irregular","Irregular"),
	NOT_PLANNED("notPlanned","Not planned"),
	UNKNOWN("unknown","Unknown");
	
	
	private String id;
	private String label;
	
}
