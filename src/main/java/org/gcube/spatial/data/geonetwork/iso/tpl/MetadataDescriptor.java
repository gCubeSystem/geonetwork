package org.gcube.spatial.data.geonetwork.iso.tpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;
import org.gcube.spatial.data.geonetwork.iso.ISOMetadataFactory;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.Language;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.MaintenanceUpdateFrequency;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.ScopeCode;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopicCategory;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.ResourceConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.DistributionInfo;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.DistributionInfo.DistributionInfoType;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.OnlineResource;
import org.gcube.spatial.data.geonetwork.iso.tpl.extent.BoundingBox;
import org.gcube.spatial.data.geonetwork.iso.tpl.extent.Extent;
import org.gcube.spatial.data.geonetwork.iso.tpl.keys.KeywordSet;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.ResponsibleParty;
import org.gcube.spatial.data.geonetwork.iso.tpl.spatial.SpatialRepresentation;
import org.gcube.spatial.data.geonetwork.iso.tpl.spatial.VectorRepresentation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class MetadataDescriptor {

	@NonNull
	private String UUIDIdentifier;
	@NonNull
	private Language language=Language.ENGLISH;
	@NonNull
	private ScopeCode scope=ScopeCode.DATASET;

	@NonNull
	private Collection<ResponsibleParty> responsibleParties=new ArrayList<>();
	
	// SPATIAL REPRESENTATION
	@NonNull
	private Date creationTime;
	
	@NonNull
	private SpatialRepresentation spatialRepresentation=new VectorRepresentation();
	@NonNull
	private Date publicationTime;
	
	
	
	@NonNull
	private String title;
	@NonNull
	private String abstractField;
	@NonNull
	private String purpose;
	@NonNull
	private HashSet<String> credits=new HashSet<>();
	
	@NonNull
	private MaintenanceUpdateFrequency maintenanceUpdateFrequency=MaintenanceUpdateFrequency.AS_NEEDED;
	
	
	@NonNull
	private ArrayList<KeywordSet> keywordSets=new ArrayList<>();
	
	@NonNull
	private ResourceConstraints constraints;
	
	
	@NonNull
	private DistributionInfo distributionInfo;
	
	@NonNull
	private Extent extent=new Extent();
	
	@NonNull
	private Double spatialResolution;
	@NonNull
	private HashSet<TopicCategory> topicCategories=new HashSet<>();
	@NonNull
	private String referenceSystem="WGS:84";
	@NonNull
	private String lineageStatement;
	
	
	public void setGeoServerDistributionInfo(String geoServerUrl,String layerName, String workspace, String style, String CRS, BoundingBox toDeclareBB) throws UriResolverMapException, IllegalArgumentException{
		List<OnlineResource> resources=new ArrayList<OnlineResource>();
		
		String wmsUrl=ISOMetadataFactory.getWmsUrl(geoServerUrl, layerName, workspace, style, toDeclareBB.toString(), CRS);
		String wcsUrl=ISOMetadataFactory.getWcsUrl(geoServerUrl, layerName, workspace, toDeclareBB.toString());
		String wfsUrl=ISOMetadataFactory.getWfsUrl(geoServerUrl, layerName, workspace);
		String gisViewerUrl=ISOMetadataFactory.getGisLinkByUUID(UUIDIdentifier);
		
		resources.add(new OnlineResource(wmsUrl, "WMS Link"));
		resources.add(new OnlineResource(wcsUrl, "WCS Link"));
		resources.add(new OnlineResource(wfsUrl, "WFS Link"));
		resources.add(new OnlineResource(gisViewerUrl, "GISViewer Link"));
		distributionInfo=new DistributionInfo(DistributionInfoType.GeoServer, resources);
	}


	public void addCredits(String toAdd) {
		getCredits().add(toAdd);
	}


	
	public void addResponsibleParty(ResponsibleParty toAdd) {
		getResponsibleParties().add(toAdd);
	}


	public void addKeywordSet(KeywordSet toAdd) {
		getKeywordSets().add(toAdd);
	}


	public void addTopicCategory(TopicCategory toAdd) {
		getTopicCategories().add(toAdd);
	}
	
	
}
