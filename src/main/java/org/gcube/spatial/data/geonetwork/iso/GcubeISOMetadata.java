package org.gcube.spatial.data.geonetwork.iso;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;
import org.gcube.spatial.data.geonetwork.iso.tpl.ISOMetadataByTemplate;
import org.gcube.spatial.data.geonetwork.iso.tpl.MetadataDescriptor;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.KeywordType;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.ResponsiblePartyRole;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.RestrictionCode;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.LegalConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.ResourceConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.DistributionInfo;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.DistributionInfo.DistributionInfoType;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.OnlineResource;
import org.gcube.spatial.data.geonetwork.iso.tpl.extent.BoundingBox;
import org.gcube.spatial.data.geonetwork.iso.tpl.keys.KeywordSet;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.Contact;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.ResponsibleParty;
import org.gcube.spatial.data.geonetwork.iso.tpl.spatial.VectorRepresentation;
import org.gcube.spatial.data.geonetwork.utils.StringValidator;
import org.geotoolkit.metadata.iso.extent.DefaultExtent;
import org.geotoolkit.xml.XML;
import org.opengis.metadata.Metadata;
import org.opengis.metadata.citation.PresentationForm;
import org.opengis.metadata.identification.TopicCategory;
import org.opengis.metadata.spatial.GeometricObjectType;
import org.opengis.metadata.spatial.TopologyLevel;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@RequiredArgsConstructor
@Deprecated
@Slf4j
public class GcubeISOMetadata {

	private EnvironmentConfiguration config;
	
	
	private MetadataDescriptor descriptor;
	
	
	@NonNull
	private String user;
	@NonNull
	private String lineageStatement;
	
	//Identification
	@NonNull
	private String title;
	@NonNull
	private Date creationDate;
	@NonNull
	private PresentationForm presentationForm;
	@NonNull
	private String abstractField;
	@NonNull
	private String purpose;
	@NonNull
	private String UUIDIdentifier;
	
	private ArrayList<String> credits=new ArrayList<String>();

	private HashMap<Thesaurus,HashSet<String>> descriptiveKeywords=new HashMap<Thesaurus, HashSet<String>>();
	
	private ArrayList<TopicCategory> topicCategories=new ArrayList<TopicCategory>();
	
	private DefaultExtent extent=(DefaultExtent) DefaultExtent.WORLD;
	
	//Spatial Representation
	
	private GeometricObjectType geometricObjectType=GeometricObjectType.SURFACE;
	
	private int geometryCount=0;
	
	private TopologyLevel topologyLevel=TopologyLevel.GEOMETRY_ONLY;
	
	private double resolution=0.5d;
	
	@NonNull
	private String inspireTheme;
	
	private ArrayList<String> graphicOverviewsURI=new ArrayList<String>(); 
	
	
	private DistributionInfo distributionInfo;
	
	private ResourceConstraints contraints=new ResourceConstraints("I'll tell you when", 
			new LegalConstraints(RestrictionCode.LICENSE,"CC-BY-SA"), 
			new LegalConstraints(RestrictionCode.LICENSE,"CC-BY-SA"));
	
	@Deprecated
	public GcubeISOMetadata() throws Exception {
		config=EnvironmentConfiguration.getConfiguration();
		credits.add(config.getProjectCitation());
		addKeyword(config.getProjectName(), config.getThesauri().get("General"));
	}
	

	protected void checkConstraints()throws MissingInformationException{
		if(!StringValidator.isValidateString(getUser())) throw new MissingInformationException("Field user is mandatory");
		if(!StringValidator.isValidateString(getTitle())) throw new MissingInformationException("Field title is mandatory");		
		if(getCreationDate()==null)throw new MissingInformationException("Field creationDate is mandatory");
		if(getPresentationForm()==null) throw new MissingInformationException("Field presentationForm is mandatory");

		if(!StringValidator.isValidateString(getAbstractField())) throw new MissingInformationException("Field abstractField is mandatory");
		if(!StringValidator.isValidateString(getPurpose())) throw new MissingInformationException("Field purpose is mandatory");
		
		if(getTopicCategories().size()==0) throw new MissingInformationException("At least one topic category is required");
		if(getExtent()==null) throw new MissingInformationException("Field Extent is mandatory");

		if(getGeometricObjectType()==null)throw new MissingInformationException("Field geometricObjectType is mandatory");
		if(getTopologyLevel()==null)throw new MissingInformationException("Field topology level is mandatory");	
		if(getCredits().size()==0) throw new MissingInformationException("At least one credits is needed");
		if(getDescriptiveKeywords().isEmpty())throw new MissingInformationException("Missing Descriptive keywords");
	}
	
	
	public File getMetadataFile() throws Exception{
		checkConstraints();		

		MetadataDescriptor desc=new MetadataDescriptor();
		ArrayList<ResponsibleParty> respParties=new ArrayList<>();		
		AuthorizationEntry authEntry = authorizationService().get(SecurityTokenProvider.instance.get());
		
		respParties.add(new ResponsibleParty(this.getUser(), "gCube Context "+authEntry.getContext(),ResponsiblePartyRole.AUTHOR));
		respParties.add(new ResponsibleParty(this.getUser(), "gCube Context "+authEntry.getContext(),ResponsiblePartyRole.POINT_OF_CONTACT));
		respParties.add(new ResponsibleParty(config.getProjectName(), config.getProjectName(), ResponsiblePartyRole.ORIGINATOR));
		respParties.add(new ResponsibleParty(config.getDistributorIndividualName(), config.getDistributorOrganisationName(), ResponsiblePartyRole.DISTRIBUTOR,new Contact(config.getDistributorEMail(),config.getDistributorSite())));
		respParties.add(new ResponsibleParty(config.getProviderIndividualName(), config.getProviderOrganisationName(), ResponsiblePartyRole.RESOURCE_PROVIDER,new Contact(config.getProviderEMail(),config.getProviderSite())));
		
		desc.setResponsibleParties(respParties);
		
		desc.setTitle(this.getTitle());
		desc.setCreationTime(this.getCreationDate());
		desc.setAbstractField(this.getAbstractField());
		
		
		for(String credit:getCredits()) {
			desc.addCredits(credit);
		}
		
		VectorRepresentation representation=new VectorRepresentation(
				org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopologyLevel.getById(this.getTopologyLevel().identifier()),
				this.getGeometryCount(),
				org.gcube.spatial.data.geonetwork.iso.tpl.codelists.GeometricObjectType.getById(this.getGeometricObjectType().identifier()));
		
		desc.setSpatialRepresentation(representation);
		
		KeywordSet keySet=new KeywordSet();
		for(Entry<Thesaurus,HashSet<String> > entry : this.getDescriptiveKeywords().entrySet()) {
			
			
			for(String s:entry.getValue()) {
				keySet.addKeyword(s);
			}
		}
		
		desc.addKeywordSet(keySet);
		
		desc.addKeywordSet(new KeywordSet(
				KeywordType.THEME,
				Collections.singleton(getInspireTheme()==null?"Species distribution":getInspireTheme()),
				org.gcube.spatial.data.geonetwork.iso.tpl.keys.Thesaurus.INSPIRE_THEMES));
		
		
//		ArrayList<Keyword> keys=new ArrayList<>();
////		for(Entry<Thesaurus,HashSet<String> > entry : this.getDescriptiveKeywords().entrySet()) {
////			Thesaurus t=entry.getKey();
////			if(t.getType().equals(KeywordType.THEME))
////			
////		}
//		desc.setKeywords(keys);

		
		desc.setPublicationTime(desc.getCreationTime());
		
		desc.setPurpose(this.getPurpose());
		
		desc.setUUIDIdentifier(UUIDIdentifier!=null?UUIDIdentifier:UUID.randomUUID().toString());
		desc.getExtent().addGeographicExtent((BoundingBox.WORLD_EXTENT));
		
		desc.setSpatialResolution(this.getResolution());
		
		
		for(TopicCategory cat:getTopicCategories())
			desc.addTopicCategory(org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopicCategory.getById(cat.identifier()));
		
		try {
			desc.setDistributionInfo(distributionInfo);
		}catch(NullPointerException e) {
			log.warn("*******Distribution info is null!!!****** This should happen only in legacy approaches.");
		}
		desc.setLineageStatement(this.getLineageStatement()!=null?this.getLineageStatement():"");
		desc.setConstraints(this.getContraints());
		
		return ISOMetadataByTemplate.createXML(desc);
	}

	
	public void setGeoServerDistributionInfo(String geoServerUrl,String layerName, String workspace, String style, String CRS) throws UriResolverMapException, IllegalArgumentException{
		List<OnlineResource> resources=new ArrayList<OnlineResource>();
		String bbox=BoundingBox.WORLD_EXTENT.toString();
		String wmsUrl=ISOMetadataFactory.getWmsUrl(geoServerUrl, layerName, workspace, style, bbox, CRS);
		String wcsUrl=ISOMetadataFactory.getWcsUrl(geoServerUrl, layerName, workspace, bbox);
		String wfsUrl=ISOMetadataFactory.getWfsUrl(geoServerUrl, layerName, workspace);
		String gisViewerUrl=ISOMetadataFactory.getGisLinkByUUID(UUIDIdentifier);
		
		resources.add(new OnlineResource(wmsUrl, "WMS Link"));
		resources.add(new OnlineResource(wcsUrl, "WCS Link"));
		resources.add(new OnlineResource(wfsUrl, "WFS Link"));
		resources.add(new OnlineResource(gisViewerUrl, "GISViewer Link"));
		distributionInfo=new DistributionInfo(DistributionInfoType.GeoServer, resources);
	}
	
	
	
	
	@Deprecated
	public Metadata getMetadata() throws Exception{		
		return (Metadata) XML.unmarshal(getMetadataFile());
	}
	
	
	// READ ONLY GETTERS AND ADDERS 
	
	
	/**
	 * @return the credits
	 */
	public ArrayList<String> getCredits() {
		return (ArrayList<String>) credits.clone();
	}

	/**
	 * Adds credits to the metadata
	 * 
	 * @param toAddCredits
	 */
	public void addCredits(String toAddCredits){
		credits.add(toAddCredits);
	}
	
	/**
	 * @return the descriptiveKeywords
	 */
	public HashMap<Thesaurus, HashSet<String>> getDescriptiveKeywords() {
		return (HashMap<Thesaurus, HashSet<String>>) descriptiveKeywords.clone();
	}

	/**
	 * Adds descriptive keywords to the metadata
	 * 
	 * @param keyword
	 * @param thesaurus
	 */
	public synchronized void addKeyword(String keyword,Thesaurus thesaurus){
		if(!descriptiveKeywords.containsKey(thesaurus)) descriptiveKeywords.put(thesaurus, new HashSet<String>());
		descriptiveKeywords.get(thesaurus).add(keyword);
	}
	
	/**
	 * @return the topicCategories
	 */
	public ArrayList<TopicCategory> getTopicCategories() {
		return (ArrayList<TopicCategory>) topicCategories.clone();
	}

	
	/**
	 * Adds a Topic Category to the metadata
	 * 
	 * @param toAdd
	 */
	public void addTopicCategory(TopicCategory toAdd){
		topicCategories.add(toAdd);
	}
	
	
	/**
	 * @return the graphicOverviewsURI
	 */
	public ArrayList<String> getGraphicOverviewsURI() {
		return (ArrayList<String>) graphicOverviewsURI.clone();
	}

	/**
	 * Adds a graphic overview uri to the metadata
	 * 
	 * @param uri
	 */
	public void addGraphicOverview(String uri){
		graphicOverviewsURI.add(uri);
	}

}
