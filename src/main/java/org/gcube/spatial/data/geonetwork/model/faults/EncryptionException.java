package org.gcube.spatial.data.geonetwork.model.faults;

public class EncryptionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8029412117818590297L;

	public EncryptionException() {
		// TODO Auto-generated constructor stub
	}

	public EncryptionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EncryptionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EncryptionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EncryptionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
