package org.gcube.spatial.data.geonetwork.model.faults;

public class AuthorizationException extends GeoNetworkException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8639478879076898891L;

	public AuthorizationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuthorizationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public AuthorizationException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public AuthorizationException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	
	
}
