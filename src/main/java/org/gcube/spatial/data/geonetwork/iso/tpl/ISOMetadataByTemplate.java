package org.gcube.spatial.data.geonetwork.iso.tpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ISOMetadataByTemplate {


	private static List<TemplateLoader> loaders=new ArrayList<TemplateLoader>();
	
	
	private static Configuration cfg;


	static {
		try{
			// Create your Configuration instance, and specify if up to what FreeMarker
			// version (here 2.3.25) do you want to apply the fixes that are not 100%
			// backward-compatible. See the Configuration JavaDoc for details.
			cfg = new Configuration(Configuration.VERSION_2_3_25);

			// Specify the source where the template files come from. Here I set a
			// plain directory for it, but non-file-system sources are possible too:

			
			
//			cfg.setDirectoryForTemplateLoading(
//					new File(ISOMetadataByTemplate.class.getResource("xmlTemplates").toURI()));

//			cfg.setClassForTemplateLoading(ISOMetadataByTemplate.class, "/xmlTemplates");
			
			// Set the preferred charset template files are stored in. UTF-8 is
			// a good choice in most applications:
			cfg.setDefaultEncoding("UTF-8");

			// Sets how errors will appear.
			// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

			// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
			cfg.setLogTemplateExceptions(false);
			
			
			loaders.add(new ClassTemplateLoader(ISOMetadataByTemplate.class, "/xmlTemplates"));
			
			
//			ISOMetadataByTemplate.class.getClassLoader().get
			reload();
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	private static final void reload() {
		MultiTemplateLoader loader=new MultiTemplateLoader(loaders.toArray(new TemplateLoader[loaders.size()]));
		cfg.setTemplateLoader(loader);
	}

	public static final File createXML(MetadataDescriptor desc) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException{
		return custom(desc,"BaseTemplate.ftlx");
//		
//		Writer out=null;
//		try{
//			Template temp = cfg.getTemplate("BaseTemplate.ftlx");
//			File output=File.createTempFile("ISO_", ".xml");
//			out=new OutputStreamWriter(new FileOutputStream(output));
//			temp.process(desc, out);
//			return output;		
//		}finally{
//			if(out!=null)
//				IOUtils.closeQuietly(out);				
//		}

	}

	public static final File custom(Object descriptor, String template) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		Writer out=null;
		try{
			Template temp = cfg.getTemplate(template);
			File output=File.createTempFile("ISO_", ".xml");
			out=new OutputStreamWriter(new FileOutputStream(output));
			temp.process(descriptor, out);
			return output;		
		}finally{
			if(out!=null)
				IOUtils.closeQuietly(out);				
		}
	}
	
	public static final void registerTemplateFolder(File baseDir) throws IOException {
		loaders.add(new FileTemplateLoader(baseDir));
		reload();
	}

}
