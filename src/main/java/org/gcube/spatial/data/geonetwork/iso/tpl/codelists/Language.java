package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Language {

	ENGLISH("eng","English"),
	FRENCH("fra","French");
	
	
	private String code;
	private String label;
	
	
	
}
