package org.gcube.spatial.data.geonetwork.iso.tpl.extent;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class BoundingBox extends GeographicExtent{

	public static final BoundingBox WORLD_EXTENT=new BoundingBox(90d, -90d, 180d, -180d);
	
	@NonNull
	private Double north;
	@NonNull
	private Double south;
	@NonNull
	private Double west;
	@NonNull
	private Double east;
	
//	public BoundingBox(double n, double s, double w, double e) {		
//		North = n;
//		South = s;
//		West = w;
//		East = e;
//	}

	
	/**
	 * 
	 * @return E, S, W, N
	 */
	public double[] toArray(){
		return new double[]{
			east,south,west,north	
		};
	}
	
	/**
	 * 
	 * @return E, S, W, N
	 */
	@Override
	public String toString() {
		return east+","+south+","+west+","+north;
	}
	
	@Override
	public Type getType() {
		return Type.BOUNDING_BOX;
	}
	
}
