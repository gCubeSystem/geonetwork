package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.Getter;

import lombok.AllArgsConstructor;

@Getter
@AllArgsConstructor
public enum KeywordType {
	DISCIPLINE("discipline"),
	PLACE("place"),
	STRATUM("stratum"),
	TEMPORAL("temporal"),
	THEME("theme");
	
	
	private final String id;
	
	public static KeywordType getById(String id) {
		for(KeywordType t:KeywordType.values())
			if(t.id.equals(id)) return t;
		throw new IllegalArgumentException("No KeywordType with id "+id);
	}
}
