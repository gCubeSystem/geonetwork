package org.gcube.spatial.data.geonetwork.iso.tpl.constraints;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ResourceConstraints {

	@NonNull
	private String useLimitation;

	@NonNull
	private LegalConstraints accessConstraints;
	@NonNull
	private LegalConstraints useConstraints;
	
	
}
