package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GeometricObjectType {

	COMPLEX("complex","Complex"),
	COMPOSITE("composite","Composite"),
	CURVE("curve","Curve"),
	POINT("point","Point"),
	SOLID("solid","Solid"),
	SURFACE("surface","Surface");
	
	
	private String id;
	private String label;
	
	
	public static GeometricObjectType getById(String id) {
		for(GeometricObjectType t:GeometricObjectType.values())
			if(t.id.equals(id)) return t;
		throw new IllegalArgumentException("No GeometricObjectType with id "+id);
	}
}
