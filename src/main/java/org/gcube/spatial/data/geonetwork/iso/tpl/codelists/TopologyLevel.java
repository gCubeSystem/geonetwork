package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TopologyLevel {

	GEOMETRY_ONLY("geometryOnly","Geometry only"),
	TOPOLOGY_1D("topology1D","Topology 1D"),
	PLANAR_GRAPH("planarGraph","Planar graph"),
	FULL_PLANAR_GRAPH("fullPlanarGraph","Full planar graph"),
	SURFACE_GRAPH("surfaceGraph","Surface graph"),
	FULL_SURFACE_GRAPH("fullSurfaceGraph","Full surface graph"),
	TOPOLOGY_3D("topology3D","Topology 3D"),
	ABSTRACT("abstract","Abstract");
	
	
	private String id;
	private String label;
	
	
	public static TopologyLevel getById(String id) {
		for(TopologyLevel t:TopologyLevel.values())
			if(t.id.equals(id)) return t;
		throw new IllegalArgumentException("No TopologyLevel with id "+id);
	}
}
