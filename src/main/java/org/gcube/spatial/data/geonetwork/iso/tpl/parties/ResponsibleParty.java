package org.gcube.spatial.data.geonetwork.iso.tpl.parties;

import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.ResponsiblePartyRole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class ResponsibleParty {

	
	@NonNull
	private String individualName;
	@NonNull
	private String organization;
	@NonNull
	private ResponsiblePartyRole role;
	
	private Contact contact;
	
	public ResponsibleParty(String individualName, String organization, ResponsiblePartyRole role) {
		super();
		this.individualName = individualName;
		this.organization = organization;
		this.role = role;		
	}
	
	
	
}
