package org.gcube.spatial.data.geonetwork.iso.tpl.codelists;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TopicCategory {
	FARMING("farming","Farming"),
	BIOTA("biota","Biota"),
	BOUNDARIES("boundaries","Boundaries"),
	CLIMATOLOGY_METEOROLOGY_ATMOSPHERE("climatologyMeteorologyAtmosphere","Climatology, Meteorology & Atmosphere"),
	ECONOMY("economy","Economy"),
	ELEVATION("elevation","Elevation"),
	ENVIRONMENT("environment","Environment"),
	GEOSCIENTIFIC_INFORMATION("geoscientificInformation","Geoscientific information"),
	HEALTH("health","Health"),
	IMAGERY_BASE_MAPS_EARTH_COVER("imageryBaseMapsEarthCover","Imagery, Base maps, Earth cover"),
	INTELLIGENCE_MILITARY("intelligenceMilitary","Intelligence, Military"),
	INLAND_WATERS("inlandWaters","Inland waters"),
	LOCATION("location","Location"),
	OCEANS("oceans","Oceans"),
	PLANNING_CADASTRE("planningCadastre","Planning cadestre"),
	SOCIETY("society","Society"),
	STRUCTURE("structure","Structure"),
	TRASNPORTATION("transportation","Transportation"),
	UTILITIES_COMMUNICATION("utilitiesCommunication","Utilities, communication");
	
	
	private String id;
	private String label;

	public static TopicCategory getById(String id) {
		for(TopicCategory t:TopicCategory.values())
			if(t.id.equals(id)) return t;
		throw new IllegalArgumentException("No TopicCategory with id "+id);
	}
}
