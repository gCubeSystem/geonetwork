package org.gcube.spatial.data.geonetwork.iso.tpl.extent;

public abstract class GeographicExtent {

	protected enum Type{
		BOUNDING_POLYGON, BOUNDING_BOX, DESCRIPTION
	}
	
	public abstract Type getType();
}
