package org.gcube.spatial.data.geonetwork.test;

import java.util.Date;

import org.gcube.spatial.data.geonetwork.GeoNetwork;
import org.gcube.spatial.data.geonetwork.GeoNetworkReader;
import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.gcube.spatial.data.geonetwork.iso.GcubeISOMetadata;
import org.gcube.spatial.data.geonetwork.iso.Thesaurus;
import org.geotoolkit.metadata.iso.extent.DefaultExtent;
import org.opengis.metadata.citation.PresentationForm;
import org.opengis.metadata.identification.TopicCategory;
import org.opengis.metadata.spatial.GeometricObjectType;
import org.opengis.metadata.spatial.TopologyLevel;

public class GcubeIsoTest {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TokenSetter.set("/gcube/devsec/devVRE");
		GeoNetworkReader reader=GeoNetwork.get();
		reader.login(LoginLevel.SCOPE);
//		System.out.println(reader.getById("0815e357-ebd7-4c02-8dc8-f945eceb870c"));
		
		System.out.println(fillMeta(0.5, "myself.self", "dumb Title", "stupid table", "BS", "no one", "keywordX").getMetadataFile());
		
	}

	
	// From ecological engine
	private static GcubeISOMetadata fillMeta(double resolution, String username, String title, String tableName, String purpose, String credits, String keyword) throws Exception {

		/*
		if (scope == null)
			scope = ScopeProvider.instance.get();
		 */
		
//		ScopeProvider.instance.set(scope);

		GcubeISOMetadata meta = new GcubeISOMetadata();
		meta.setAbstractField("This metadata has been automatically generated from the Statistical Manager on the basis of a distribution of points and according the resolution of " + resolution + " degrees.");
		meta.setCreationDate(new Date(System.currentTimeMillis()));
		meta.setExtent((DefaultExtent) DefaultExtent.WORLD);
		meta.setGeometricObjectType(GeometricObjectType.SURFACE);
		meta.setPresentationForm(PresentationForm.MAP_DIGITAL);
		meta.setPurpose(purpose);
		meta.setResolution(resolution);
		if (title == null || title.length() == 0)
			meta.setTitle("Distribution");
		else
			meta.setTitle(title);

		meta.setTopologyLevel(TopologyLevel.GEOMETRY_ONLY);
		meta.setUser(username);

		meta.addGraphicOverview("http://www.d4science.org/D4ScienceOrg-Social-theme/images/custom/D4ScienceInfrastructure.png");
		meta.addCredits(credits);
		Thesaurus generalThesaurus = meta.getConfig().getThesauri().get("General");
		meta.addKeyword(title, generalThesaurus);
		meta.addKeyword(username, generalThesaurus);
		meta.addKeyword("DataMiner", generalThesaurus);
		meta.addKeyword(keyword, generalThesaurus);
		meta.addKeyword(tableName, generalThesaurus);
		meta.addTopicCategory(TopicCategory.BIOTA);
		return meta;
	}
}
