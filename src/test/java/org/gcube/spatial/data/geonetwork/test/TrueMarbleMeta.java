package org.gcube.spatial.data.geonetwork.test;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.gcube.spatial.data.geonetwork.iso.tpl.ISOMetadataByTemplate;
import org.gcube.spatial.data.geonetwork.iso.tpl.MetadataDescriptor;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.GeometricObjectType;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.KeywordType;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.ResponsiblePartyRole;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.RestrictionCode;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopicCategory;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopologyLevel;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.LegalConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.ResourceConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.extent.BoundingBox;
import org.gcube.spatial.data.geonetwork.iso.tpl.keys.KeywordSet;
import org.gcube.spatial.data.geonetwork.iso.tpl.keys.Thesaurus;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.Contact;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.ResponsibleParty;
import org.gcube.spatial.data.geonetwork.iso.tpl.spatial.VectorRepresentation;

import it.geosolutions.geonetwork.util.GNInsertConfiguration;

public class TrueMarbleMeta {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TokenSetter.set("/gcube/devsec/devVRE");
//		TokenSetter.set("/pred4s/preprod/preVRE");		
//		TokenSetter.set("/d4science.research-infrastructures.eu");
		
		
		// GCUBE OBJECT
		
//		GcubeISOMetadata gMeta=new GcubeISOMetadata();
//		gMeta.setAbstractField("This layer is used as a base layer for GIS VIewer widget");
//		gMeta.setCreationDate(new Date(System.currentTimeMillis()));
//		gMeta.setExtent((DefaultExtent) DefaultExtent.WORLD);
//		gMeta.setGeometricObjectType(GeometricObjectType.SURFACE);
//		gMeta.setPresentationForm(PresentationForm.IMAGE_DIGITAL);
//		gMeta.setPurpose(gMeta.getAbstractField());
//		gMeta.setTitle("TrueMarble_"+ScopeUtils.getCurrentScope()+"_test");
//		gMeta.setUser("fabio.sinibaldi");
//		Thesaurus general=gMeta.getConfig().getThesauri().get("General");
//		gMeta.addKeyword("True Marble", general);
//		gMeta.addTopicCategory(TopicCategory.ENVIRONMENT);
//		
//		Metadata meta=gMeta.getMetadata();
//		
//		((DefaultMetadata)meta).setDistributionInfo(ISOMetadataFactory.getDistributionByLayer("TrueMarble.16km.2700x1350", "http://geoserver-dev.d4science.org/geoserver", "raster", "-180.0,-90.0,180.0,90.0", gMeta.getConfig()));
//		XML.marshal(meta, new File("TrueMarble.xml"));
		
		
		// VASSILIS
		
		
		
		//Long id=publisher.insertMetadata(config,new File("/tmp/GEO_1069334659927122420.xml"));
//		try{
//			Long id=publisher.insertMetadata(config,meta);
//			System.out.println("Inserted meta with id : "+id);
//		}catch(Throwable t) {
//			try{
//				System.out.println("Trying with file...");
//			File f=gMeta.getMetadataFile();
//			
//			System.out.println("Going to publish : "+f.getAbsolutePath());
//			System.out.println("Inserted meta with id : "+publisher.insertMetadata(config,f));
//			}catch(Throwable t1) {
//				System.err.println("Unable to push meta : ");
//			
//				t1.printStackTrace(System.err);
//			}
//		}
		
		
		
		
		
		
		
		
	
		// TEMPLATES
		
		MetadataDescriptor desc=new MetadataDescriptor();
		desc.setResponsibleParties(Arrays.asList(
				new ResponsibleParty("The scope ","the infra",ResponsiblePartyRole.DISTRIBUTOR,new Contact("my.mail@this.place.com","www.mipiacitu.com")),
				new ResponsibleParty("Io","me stesso",ResponsiblePartyRole.POINT_OF_CONTACT,new Contact("point.of.contact.maiol@place.com","www.mipiacitu.com")),
				new ResponsibleParty("Io","me stesso",ResponsiblePartyRole.AUTHOR,new Contact("point.of.contact.maiol@place.com","www.mipiacitu.com"))
				));
		
		
		desc.setAbstractField("My Abstract Field");
		
		desc.addCredits("Fatto io");		
		
		desc.setCreationTime(new GregorianCalendar().getTime());
		VectorRepresentation representation=new VectorRepresentation(
				TopologyLevel.GEOMETRY_ONLY, 1000, GeometricObjectType.POINT);
				
		
		desc.setSpatialRepresentation(representation);
		
		desc.addKeywordSet(new KeywordSet(
				KeywordType.THEME,
				Collections.singleton("Species distribution"),
				Thesaurus.INSPIRE_THEMES));
		
		desc.addKeywordSet(new KeywordSet(Collections.singleton("My keyword")));
		
		
		desc.setPurpose("Just for fun");
		desc.setTitle("Il mio bel titolone");
		desc.setUUIDIdentifier(UUID.randomUUID().toString());
		desc.getExtent().addGeographicExtent(BoundingBox.WORLD_EXTENT);
		
//		desc.setGeoServerDistributionInfo("http://geoserver.d4science.org/geoserver", "ws","wmpa", "speciesProb", "EPSG:4326",BoundingBox.WORLD_EXTENT);
		
		
		desc.setSpatialResolution(0.5d);
		desc.setConstraints(new ResourceConstraints("I'll tell you when", 
				new LegalConstraints(RestrictionCode.LICENSE,"CC-BY-SA"), 
				new LegalConstraints(RestrictionCode.LICENSE,"CC-BY-SA")));
		
		desc.addTopicCategory(TopicCategory.ENVIRONMENT);
		desc.setLineageStatement("I made with my own hands");
		
		
		File metaFile=ISOMetadataByTemplate.createXML(desc);
		System.out.println("Going to Publish ----->> "+metaFile.getAbsolutePath());
		
		GeoNetworkPublisher publisher=TestConfiguration.getClient();
		publisher.login(LoginLevel.PRIVATE);
		GNInsertConfiguration config=publisher.getCurrentUserConfiguration("dataset", "_none_");
		
		config.setValidate(false);
		System.out.println("Settings : "+publisher.getConfiguration().getScopeConfiguration());
		System.out.println("Publishing Settings : validate = "+config.getValidate());
		long id=publisher.insertMetadata(config, metaFile);
		System.out.println("PUBLISHED WITH ID : "+id);
		
		

		
		
	}

}
