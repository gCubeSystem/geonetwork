package org.gcube.spatial.data.geonetwork.test;

import java.io.File;
import java.util.Date;

import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.gcube.spatial.data.geonetwork.iso.GcubeISOMetadata;
import org.gcube.spatial.data.geonetwork.iso.Thesaurus;
import org.geotoolkit.metadata.iso.extent.DefaultExtent;
import org.opengis.metadata.citation.PresentationForm;
import org.opengis.metadata.identification.TopicCategory;
import org.opengis.metadata.spatial.GeometricObjectType;
import org.opengis.metadata.spatial.TopologyLevel;

import it.geosolutions.geonetwork.util.GNInsertConfiguration;

public class VassilisTest {

	public static void main(String[] args) throws Exception{
		TokenSetter.set("/gcube/devNext");
		GcubeISOMetadata gMeta=new GcubeISOMetadata();
		 gMeta.setAbstractField("Layer from geopolis");
		    gMeta.setCreationDate(new Date());
		    gMeta.setExtent((DefaultExtent) DefaultExtent.WORLD);
		    gMeta.setGeometricObjectType(GeometricObjectType.SURFACE);
		    gMeta.setPresentationForm(PresentationForm.valueOf(PresentationForm.MAP_DIGITAL.name()));
		    gMeta.setResolution(1.2);
		    gMeta.setPurpose("Test library");
		    gMeta.setTitle("Title");
		    gMeta.setTopologyLevel(TopologyLevel.GEOMETRY_ONLY);
		    gMeta.setUser("Some user");

		    gMeta.addCredits("Team of geospatialists");
		    gMeta.addGraphicOverview("http://localhost:8080/wms/something");

		    Thesaurus generalThesaurus = gMeta.getConfig().getThesauri().get("General");
		    gMeta.addKeyword("key1", generalThesaurus);
		    gMeta.addKeyword("key2", generalThesaurus);
		    
		    gMeta.setInspireTheme("Habitats and biotopes");
		    
		    gMeta.addTopicCategory(TopicCategory.ENVIRONMENT);
		    gMeta.setGeoServerDistributionInfo("http://geoserver.d4science.org/geoserver", "ws","wmpa", "speciesProb", "EPSG:4326");
		    
		    gMeta.setLineageStatement("I made with my own hands");
		    
		File metaFile=gMeta.getMetadataFile();
		
		System.out.println("Going to Publish ----->> "+metaFile.getAbsolutePath());
		
		GeoNetworkPublisher publisher=TestConfiguration.getClient();
		publisher.login(LoginLevel.SCOPE);
		GNInsertConfiguration config=publisher.getCurrentUserConfiguration("dataset", "_none_");
		config.setValidate(true);
		
		
		System.out.println("Settings : "+publisher.getConfiguration().getScopeConfiguration());
		System.out.println("Publishing Settings : validate = "+config.getValidate());
		long id=publisher.insertMetadata(config, metaFile);
		System.out.println("PUBLISHED WITH ID : "+id);
	}

}
