package org.gcube.spatial.data.geonetwork.test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.UUID;

import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;
import org.gcube.spatial.data.geonetwork.GeoNetwork;
import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.gcube.spatial.data.geonetwork.iso.tpl.ISOMetadataByTemplate;
import org.gcube.spatial.data.geonetwork.iso.tpl.MetadataDescriptor;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.GeometricObjectType;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.KeywordType;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.ResponsiblePartyRole;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.RestrictionCode;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopicCategory;
import org.gcube.spatial.data.geonetwork.iso.tpl.codelists.TopologyLevel;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.LegalConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.constraints.ResourceConstraints;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.DistributionInfo;
import org.gcube.spatial.data.geonetwork.iso.tpl.distribution.DistributionInfo.DistributionInfoType;
import org.gcube.spatial.data.geonetwork.iso.tpl.extent.BoundingBox;
import org.gcube.spatial.data.geonetwork.iso.tpl.keys.KeywordSet;
import org.gcube.spatial.data.geonetwork.iso.tpl.keys.Thesaurus;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.Contact;
import org.gcube.spatial.data.geonetwork.iso.tpl.parties.ResponsibleParty;
import org.gcube.spatial.data.geonetwork.iso.tpl.spatial.VectorRepresentation;
import org.gcube.spatial.data.geonetwork.model.faults.MissingConfigurationException;
import org.gcube.spatial.data.geonetwork.model.faults.MissingServiceEndpointException;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import it.geosolutions.geonetwork.exception.GNLibException;
import it.geosolutions.geonetwork.exception.GNServerException;
import it.geosolutions.geonetwork.util.GNInsertConfiguration;

public class TestScavo {

	public static void main(String[] args) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, UriResolverMapException, IllegalArgumentException, IOException, TemplateException, Exception, GNLibException, GNServerException, MissingServiceEndpointException {
		
		//Login
		TokenSetter.set("/d4science.research-infrastructures.eu/D4OS/GeoNA-Prototype");
		GeoNetworkPublisher pub=GeoNetwork.get();
		pub.login(LoginLevel.DEFAULT);
		//obtainMeta();
		//Push
		GNInsertConfiguration conf=pub.getCurrentUserConfiguration("dataset", "_none_");
		conf.setValidate(false);
		System.out.println(pub.insertMetadata(new File("/Users/FabioIsti/Documents/Progetto_Faenza.xml")));
		
	}
	
	public static void obtainMeta() throws UriResolverMapException, IllegalArgumentException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		MetadataDescriptor desc=new MetadataDescriptor();
		
		// Nome indicativo del progetto di scavo
		
		String projectName="Progetto Faventia";
		
		
		// Introduction :  descrizione del progetto e dei suoi obiettivi a brief summary (max. 200-300 words) 
		// of the main aims and objectives of the project (or alternative process) from which the data collection arose
		String introduction="Il progetto Faventia si propone di studiare, mappare e analizzare le evidenze archeologiche presenti nel territorio basso faentino al fine di ricostruire gli assetti del popolamento antico e valorizzare il patrimonio storico e archeologico locale, incrementandone la conoscenza sia a livello qualitativo che quantitativo.";
		
		desc.setPurpose(introduction);
		
		
		//Overview : descrizione dei contenuti del progettoa summary description (max. 200-600 words) 
		//of the content of the dataset.
		String overview="Relazione di fine ricognizione e relativo abstract; selezione di immagini rappresentative; posizionamento topografico dell’area indagata; elenco e posizionamento delle UT; elenco e posizionamento dei siti";
		
		desc.setAbstractField(overview);
		
		// Preview Images

		//TODO
		
		
		
		
		// Related Resources
		
		//TODO
		
		
		// Project dates
			// Field fork start / end
		//TODO
		
		
		//Project Funders : Istituti che finanziano il progetto
		//TODO
		
		// License
		// Copyright Holder : name of the copyright holder for the dataset. It is possible that the collection will 
		// have more than one copyright holder, please list them all. 
		// If the collection was created during your work as an employee, 
		// the copyright holder will normally be your employer under your contract of employment.
		
		
		//License Holder : the licence holder is the individual, organisation, or the person who will sign a deposit 
		// licence on behalf of an organisation. This is usually the same as the main copyright holder. 
		// Include a valid email address as the e-licence will be sent to this address.
		
		
		
		// Creator: the individuals, organisations or individuals working on behalf of organisations 
		// that the collection should be attributed to.

		
		//EXCEL "Authore": Direttore scientifico: prof. Andrea Augenti (andrea.augenti@unibo.it - Università di Bologna)   
		// Coordinamento scientifico: dott. Marco Cavalazzi (marco.cavalazzi3@unibo.it - Università di Bologna) 
		// Responsabili sul campo: dott. Michele Abballe (michele.abballe@ugent.be - Ghent University, BE); 
		// dott.ssa Michela De Felicibus (michela.defelicibus2@unibo.it - Università di Bologna)
		
		desc.setResponsibleParties(Arrays.asList(
				new ResponsibleParty("Prof. Andrea Augenti","Università di Bologna",ResponsiblePartyRole.POINT_OF_CONTACT,new Contact("andrea.augenti@unibo.it","")),
				new ResponsibleParty("Dott. Marco Cavalazzi","Università di Bologna",ResponsiblePartyRole.AUTHOR,new Contact("marco.cavalazzi3@unibo.it","")),
				new ResponsibleParty("Dott. Michele Abballe","Ghent University. BE",ResponsiblePartyRole.AUTHOR,new Contact("michele.abballe@ugent.be","")),
				new ResponsibleParty("Dott.ssa Michela De Felicibus","Università di Bologna",ResponsiblePartyRole.AUTHOR,new Contact("michela.defelicibus@unibo.it","")),
				new ResponsibleParty("Geoportale Nazionale per l'Archeologia","MiBAC",ResponsiblePartyRole.CUSTODIAN,new Contact("","")),
				new ResponsibleParty("","Università di Bologna",ResponsiblePartyRole.PUBLISHER,new Contact("","")),
				new ResponsibleParty("GeoNA-Proto","D4Science",ResponsiblePartyRole.RESOURCE_PROVIDER,new Contact("fabio.sinibaldi@isti.cnr.it","www.d4science.org"))
				));
		
		
		// Keywords:
		// Free Keyword :
		//	Period Keyword : (i.e. Mesolitico, Neolitico, Età del Bronzo)

		desc.addKeywordSet(new KeywordSet(new HashSet<String>(Arrays.asList(new String[] {
				"Età del Bronzo", "Età del Ferro", "Età Romana", 
				"Età Tardoantica", "Alto Medioevo", "Basso Medioevo"
		}))));
		
		
		// Attachments : 
//		*Relazione di scavo (pdf)
//		 Title (generated : <Project Name>+ “relazione di scavo”)
//		 Abstract
		
		desc.setGeoServerDistributionInfo("http://geoserver.d4science.org/geoserver", "geona-proto","posizionamento_area", "speciesProb", "EPSG:4326",BoundingBox.WORLD_EXTENT);
			
		
//		*Immagini Rappresentative (TIFF / JPEG)
//		 Title
//		 Caption
		
		
//		*Posizionamento dell’area (GIS)
//		 Title (generated : <Project Name>+ “posizionamento dell’area”)
//		 Description
//		 Data Quality Assessment
//		 Method of Data Capture
//		*Pianta di fine scavo (GIS)
//		 Title (generated : <Project Name>+ “pianta di fine scavo”)
//		 Description
//		 Data Quality Assessment : Here you can provide an assessment or statement about the quality and accuracy of the data used and/or created within the GIS (i.e. “None” or “High risk of spatial dispacement”)
//		 Method of Data Capture
//		*Diagramma Stratigrafico (pdf)
//		 Title (generated : <Project Name>+ “diagramma stratigrafico”)
//		 Abstract
//		*Elenco Materiali (xls)
//		 Title (generated : <Project Name>+ “elenco materiali”)
//		 Abstract
//		*Schede di Catalogo reperti integri o più rappresentativi (xls)
//		 Title (generated : <Project Name>+ “Schede catalogo reperti integri o più rappresentativi”)
//		*Schede sepoltura etc. (pdf)
//		 Title
//		 Abstract

		
		
		
		
		
		System.out.println("FILE :"+ISOMetadataByTemplate.createXML(desc).getAbsolutePath());
		
	}
}
