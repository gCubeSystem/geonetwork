This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.geonetwork

## [v3.4.5] - 2020-11-16
Changed maven repositories in pom

### Fixes
- Change of nexus hostname in pom.xml

## [v3.4.4] - 2020-05-15

### Fixes
- gCube bom 2.0.0 integration (https://support.d4science.org/issues/19612)
- NOT NULL constraints removed from legacy methods (https://support.d4science.org/issues/19965)

## [v3.4.3] - 2020-05-15

### Enhancements

- ISOMetadataByTemplate now exposes a method to define custom freemarker templates